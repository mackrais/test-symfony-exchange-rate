<?php
/**
 * Created by PhpStorm.
 * PHP Version: 8.2.
 *
 * @category   <ConsoleCommand>
 *
 * @author     Oleh Boiko <developer@mackrais.com>
 * @copyright  2014-2023 @MackRais
 *
 * @see       <https://mackrais.com>
 * @date      23.12.23
 */

declare(strict_types=1);

namespace App\Command;

use App\Enum\Bank;
use App\Enum\CurrencyIsoCode;
use App\Service\Bank\BankService;
use App\Service\Notification\NotificationService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'mc:sync:exchange:rate',
    description: 'Sync exchange rate from banks (privat24, monobank)',
)]
class SyncExchangeRateCommand extends Command
{

    public function __construct(
        private readonly BankService $bankService,
        private readonly NotificationService $notificationService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $listCurrency = [CurrencyIsoCode::USD, CurrencyIsoCode::EUR];
        $listBanks = Bank::cases();

        foreach ($listBanks as $bankEnum){
            $bank = $this->bankService->getBank($bankEnum);
            $listExchangeRates = $bank->getList(...$listCurrency);
            $thresholdData = $bank->getThresholdData($listExchangeRates);
            if(!empty($thresholdData)){
                $this->notificationService->sendExchangeRate($thresholdData, 'Exchange Rate ' . $bankEnum->name);
            }
            $bank->clearHistory();
            $bank->saveHistory($listExchangeRates);
        }

        return self::SUCCESS;
    }
}
