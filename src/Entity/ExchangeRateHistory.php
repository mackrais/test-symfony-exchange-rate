<?php
/**
 * Created by PhpStorm.
 * PHP Version: 8.2.
 *
 * @category   <Entity>
 *
 * @author     Oleh Boiko <developer@mackrais.com>
 * @copyright  2014-2023 @MackRais
 *
 * @see       <https://mackrais.com>
 * @date      23.12.23
 */

namespace App\Entity;

use App\Enum\Bank;
use App\Enum\CurrencyIsoCode;
use App\Repository\ExchangeRateHistoryRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ExchangeRateHistoryRepository::class)]
class ExchangeRateHistory
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: 'string', enumType: Bank::class)]
    private Bank $bank;

    #[ORM\Column(type: 'string', enumType: CurrencyIsoCode::class)]
    private CurrencyIsoCode $isoCode;

    #[ORM\Column(type: 'integer')]
    private int $buy;

    #[ORM\Column(type: 'integer')]
    private int $sale;

    #[ORM\Column(type: "datetime")]
    private \DateTime $updateAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBank(): Bank
    {
        return $this->bank;
    }

    public function setBank(Bank $bank): static
    {
        $this->bank = $bank;

        return $this;
    }

    public function getBuy(): int
    {
        return $this->buy;
    }

    /**
     * @param int $buy
     * @return ExchangeRateHistory
     */
    public function setBuy(int $buy): self
    {
        $this->buy = $buy;

        return $this;
    }

    public function getSale(): int
    {
        return $this->sale;
    }

    /**
     * @param int $sale
     * @return ExchangeRateHistory
     */
    public function setSale(int $sale): self
    {
        $this->sale = $sale;

        return $this;
    }

    public function getUpdateAt(): \DateTime
    {
        return $this->updateAt;
    }

    public function setUpdateAt(): self
    {
        $this->updateAt = new DateTime("now");
        return $this;
    }

    public function getIsoCode(): CurrencyIsoCode
    {
        return $this->isoCode;
    }

    /**
     * @param CurrencyIsoCode $isoCode
     * @return ExchangeRateHistory
     */
    public function setIsoCode(CurrencyIsoCode $isoCode): self
    {
        $this->isoCode = $isoCode;

        return $this;
    }
}
