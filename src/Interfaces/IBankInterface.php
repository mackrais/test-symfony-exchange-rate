<?php
/**
 * Created by PhpStorm.
 * PHP Version: 8.2.
 *
 * @category   <Interface>
 *
 * @author     Oleh Boiko <developer@mackrais.com>
 * @copyright  2014-2023 @MackRais
 *
 * @see       <https://mackrais.com>
 * @date      23.12.23
 */

declare(strict_types=1);

namespace App\Interfaces;

use App\Dto\BankExchangeRate;
use App\Dto\BankExchangeRateThreshold;
use App\Enum\CurrencyIsoCode;

interface IBankInterface
{
    public function getList(CurrencyIsoCode ...$currency);

    public function saveHistory(array $exchangeRates): void;

    public function clearHistory(): void;

    /**
     * @param BankExchangeRate[] $exchangeRates
     * @return BankExchangeRateThreshold[]
     */
    public function getThresholdData(array $exchangeRates): array;
}
