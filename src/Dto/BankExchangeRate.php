<?php
/**
 * Created by PhpStorm.
 * PHP Version: 8.2.
 *
 * @category   <Dto>
 *
 * @author     Oleh Boiko <developer@mackrais.com>
 * @copyright  2014-2023 @MackRais
 *
 * @see       <https://mackrais.com>
 * @date      23.12.23
 */

declare(strict_types=1);

namespace App\Dto;

use App\Enum\CurrencyIsoCode;

class BankExchangeRate
{
    public function __construct(public CurrencyIsoCode $isoCode, public int $buy, public int $sale)
    {
    }
}
