<?php
/**
 * Created by PhpStorm.
 * PHP Version: 8.2.
 *
 * @category   <Service>
 *
 * @author     Oleh Boiko <developer@mackrais.com>
 * @copyright  2014-2023 @MackRais
 *
 * @see       <https://mackrais.com>
 * @date      23.12.23
 */

declare(strict_types=1);

namespace App\Service\Bank;

use App\Enum\Bank;
use App\Interfaces\IBankInterface;
use App\Repository\ExchangeRateHistoryRepository;
use Psr\Log\InvalidArgumentException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

readonly class BankService
{
    public function __construct(
        private HttpClientInterface $httpClient,
        private ExchangeRateHistoryRepository $repository,
        private ParameterBagInterface $parameterBag,
    ) {
    }

    public function getBank(Bank $name): IBankInterface
    {
        if (!method_exists($this, $name->value)) {
            throw new InvalidArgumentException('Unknown bank '.$name->value);
        }

        return $this->{$name->value}();
    }

    protected function privatBank(): IBankInterface
    {
        return new PrivatBank($this->httpClient, $this->repository, $this->parameterBag);
    }

    protected function monoBank(): IBankInterface
    {
        return new MonoBank($this->httpClient, $this->repository, $this->parameterBag);
    }
}
