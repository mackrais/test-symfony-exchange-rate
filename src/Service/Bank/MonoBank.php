<?php
/**
 * Created by PhpStorm.
 * PHP Version: 8.2.
 *
 * @category   <Service>
 *
 * @author     Oleh Boiko <developer@mackrais.com>
 * @copyright  2014-2023 @MackRais
 *
 * @see       <https://mackrais.com>
 * @date      23.12.23
 */

declare(strict_types=1);

namespace App\Service\Bank;

use App\Dto\BankExchangeRate;
use App\Dto\BankExchangeRateThreshold;
use App\Enum\Bank;
use App\Enum\CurrencyIsoCode;
use App\Interfaces\IBankInterface;
use Exception;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Parser\DecimalMoneyParser;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class MonoBank extends AbstractBank implements IBankInterface
{

    public function getList(?CurrencyIsoCode ...$currencies): array
    {
        $data = $this->getApiData();
        if (empty($currencies)) {
            return $this->transform($data);
        }
        $currenciesIsoCode = array_map(fn($rate) => $rate->value, $currencies);

        $filteredList = array_filter(
            $data,
            function ($item) use ($currenciesIsoCode) {
                $currencyCodeA = (string)($item['currencyCodeA'] ?? 'unknown');
                $currencyCodeB = (string)($item['currencyCodeB'] ?? 'unknown');

                return in_array($currencyCodeA, $currenciesIsoCode) && $currencyCodeB === CurrencyIsoCode::UAH->value;
            }
        );

        return $this->transform($filteredList);
    }

    public function clearHistory(): void
    {
        $this->repository->clearBankHistory($this->getBank());
    }

    /**
     * @param BankExchangeRate[] $exchangeRates
     * @return void
     */
    public function saveHistory(array $exchangeRates): void
    {
        $this->repository->saveBankHistory($exchangeRates, $this->getBank());
    }

    /**
     * @param BankExchangeRate[] $exchangeRates
     * @return BankExchangeRateThreshold[]
     * @throws Exception
     */
    public function getThresholdData(array $exchangeRates): array
    {
        return $this->prepareThresholdData($exchangeRates, $this->getBank());
    }

    protected function getBank(): Bank
    {
        return Bank::MONO_BANK;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    protected function getApiData(): array
    {
        return $this->fetchRates($this->parameterBag->get('MONOBANK_ENDPOINT'));
    }


    /**
     * @param array $payload
     * @return BankExchangeRate[]
     */
    protected function transform(array $payload): array
    {
        $currencies = new ISOCurrencies();
        $pars = new DecimalMoneyParser($currencies);
        $data = [];
        foreach ($payload as $item) {
            $currencyIsoCode = CurrencyIsoCode::fromISO4217((string)($item['currencyCodeA'] ?? 'unknown'));
            $data[$currencyIsoCode->name] = new BankExchangeRate(
                $currencyIsoCode,
                +$pars->parse((string)($item['rateBuy'] ?? 0), new Currency(CurrencyIsoCode::UAH->name))->getAmount(),
                +$pars->parse((string)($item['rateSell'] ?? 0), new Currency(CurrencyIsoCode::UAH->name))->getAmount(),
            );
        }

        return $data;
    }
}
