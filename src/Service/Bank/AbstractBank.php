<?php
/**
 * Created by PhpStorm.
 * PHP Version: 8.2.
 *
 * @category   <Service>
 *
 * @author     Oleh Boiko <developer@mackrais.com>
 * @copyright  2014-2023 @MackRais
 *
 * @see       <https://mackrais.com>
 * @date      23.12.23
 */

declare(strict_types=1);

namespace App\Service\Bank;

use App\Dto\BankExchangeRate;
use App\Dto\BankExchangeRateThreshold;
use App\Enum\Bank;
use App\Repository\ExchangeRateHistoryRepository;
use Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AbstractBank
{
    public function __construct(
        protected HttpClientInterface $httpClient,
        protected ExchangeRateHistoryRepository $repository,
        protected ParameterBagInterface $parameterBag
    ) {
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    protected function fetchRates(string $apiUrl): array
    {
        $response = $this->httpClient->request('GET', $apiUrl);

        return $response->toArray();
    }

    public function getThreshold(): int
    {
        $threshold = $this->parameterBag->get('EXCHANGE_RATE_THRESHOLD');
        if (!$threshold) {
            throw new Exception('Need set EXCHANGE_RATE_THRESHOLD');
        }

        return (int)$threshold;
    }


    /**
     * @param BankExchangeRate[] $exchangeRates
     * @return BankExchangeRateThreshold[]
     * @throws Exception
     */
    public function prepareThresholdData(array $exchangeRates, Bank $bank): array
    {
        $history = $this->repository->getByBank($bank);
        $data = [];
        $threshold = $this->getThreshold();

        foreach ($history as $item) {
            $currencyCode = $item->getIsoCode()->name;
            $newExchangeRate = $exchangeRates[$currencyCode] ?? null;
            if (!$newExchangeRate) {
                continue;
            }
            $buyThreshold = $newExchangeRate->buy - $item->getBuy() ;
            $saleThreshold =  $newExchangeRate->sale - $item->getSale();
            $hasChanged = $buyThreshold > $threshold || ($threshold * -1) > $buyThreshold || $saleThreshold > $threshold || ($threshold * -1) > $saleThreshold;

            if ($hasChanged) {
                $data[] = new BankExchangeRateThreshold(
                    $newExchangeRate->isoCode,
                    $newExchangeRate->buy,
                    $newExchangeRate->sale,
                    $buyThreshold,
                    $saleThreshold,
                );
            }
        }

        return $data;
    }
}
