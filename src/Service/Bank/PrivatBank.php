<?php
/**
 * Created by PhpStorm.
 * PHP Version: 8.2.
 *
 * @category   <Service>
 *
 * @author     Oleh Boiko <developer@mackrais.com>
 * @copyright  2014-2023 @MackRais
 *
 * @see       <https://mackrais.com>
 * @date      23.12.23
 */


declare(strict_types=1);

namespace App\Service\Bank;

use App\Dto\BankExchangeRate;
use App\Dto\BankExchangeRateThreshold;
use App\Enum\Bank;
use App\Enum\CurrencyIsoCode;
use App\Interfaces\IBankInterface;
use Exception;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Parser\DecimalMoneyParser;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class PrivatBank extends AbstractBank implements IBankInterface
{

    public function getList(?CurrencyIsoCode ...$currencies): array
    {
        $data = $this->getApiData();
        if (empty($currencies)) {
            return $this->transform($data);
        }
        $currenciesIsoCode = array_map(fn($rate) => $rate->name, $currencies);
        $filteredList = array_filter($data,
            fn($item) => in_array((mb_strtoupper($item['ccy'] ?? 'unknown')), $currenciesIsoCode));

        return $this->transform($filteredList);
    }

    public function clearHistory(): void
    {
        $this->repository->clearBankHistory($this->getBank());
    }

    /**
     * @param BankExchangeRate[] $exchangeRates
     * @return void
     */
    public function saveHistory(array $exchangeRates): void
    {
        $this->repository->saveBankHistory($exchangeRates, $this->getBank());
    }

    /**
     * @param BankExchangeRate[] $exchangeRates
     * @return BankExchangeRateThreshold[]
     * @throws Exception
     */
    public function getThresholdData(array $exchangeRates): array
    {
        return $this->prepareThresholdData($exchangeRates, $this->getBank());
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    protected function getApiData(): array
    {
        return $this->fetchRates($this->parameterBag->get('PRIVAT_BANK_ENDPOINT'));
    }

    protected function getBank(): Bank
    {
        return Bank::PRIVAT_BANK;
    }

    /**
     * @param array $payload
     * @return BankExchangeRate[]
     */
    protected function transform(array $payload): array
    {
        $currencies = new ISOCurrencies();
        $pars = new DecimalMoneyParser($currencies);
        $data = [];
        foreach ($payload as $item) {
            $currencyIsoCode = CurrencyIsoCode::tryFromName(mb_strtoupper($item['ccy'] ?? 'unknown'));
            $data[$currencyIsoCode->name] = new BankExchangeRate(
                $currencyIsoCode,
                +$pars->parse((string)($item['buy'] ?? 0), new Currency(CurrencyIsoCode::UAH->name))->getAmount(),
                +$pars->parse((string)($item['sale'] ?? 0), new Currency(CurrencyIsoCode::UAH->name))->getAmount(),
            );
        }

        return $data;
    }
}
