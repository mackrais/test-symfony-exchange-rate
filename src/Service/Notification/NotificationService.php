<?php
/**
 * Created by PhpStorm.
 * PHP Version: 8.2.
 *
 * @category   <Service>
 *
 * @author     Oleh Boiko <developer@mackrais.com>
 * @copyright  2014-2023 @MackRais
 *
 * @see       <https://mackrais.com>
 * @date      23.12.23
 */

declare(strict_types=1);

namespace App\Service\Notification;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class NotificationService
{
    public function __construct(
        private readonly MailerInterface $mailer,
        private readonly ParameterBagInterface $parameterBag
    ) {
    }

    public function sendExchangeRate(array $exchangeRates, string $subject): void
    {
        $email = (new TemplatedEmail())
            ->from($this->parameterBag->get('MAILER_NO_REPLY_EMAIL'))
            ->to(new Address($this->parameterBag->get('EXCHANGE_RATE_NOTIFICATION_RECIPIENT')))
            ->subject($subject)
            ->htmlTemplate('email/exchange-rates.html.twig')
            ->context([
                'exchangeRates' => $exchangeRates,
            ]);

        $this->mailer->send($email);
    }
}
