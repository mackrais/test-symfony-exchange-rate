<?php
/**
 * Created by PhpStorm.
 * PHP Version: 8.2.
 *
 * @category   <TwigFilter>
 *
 * @author     Oleh Boiko <developer@mackrais.com>
 * @copyright  2014-2023 @MackRais
 *
 * @see       <https://mackrais.com>
 * @date      23.12.23
 */

declare(strict_types=1);

namespace App\Twig\Filter;

use App\Enum\CurrencyIsoCode;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Formatter\IntlMoneyFormatter;
use Money\Money;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppFilterExtension extends AbstractExtension
{

    public function getFilters(): array
    {
        return [
            new TwigFilter('mc_money', [$this, 'mcMoney']),
        ];
    }

    public function mcMoney($amount, ?CurrencyIsoCode $isoCode = CurrencyIsoCode::UAH): string
    {
        $money = new Money($amount, new Currency($isoCode->name));
        $currencies = new ISOCurrencies();

        $numberFormatter = new \NumberFormatter('en_US', \NumberFormatter::CURRENCY);
        $moneyFormatter = new IntlMoneyFormatter($numberFormatter, $currencies);

        return $moneyFormatter->format($money);
    }

}
