<?php
/**
 * Created by PhpStorm.
 * PHP Version: 8.2.
 *
 * @category   <Repository>
 *
 * @author     Oleh Boiko <developer@mackrais.com>
 * @copyright  2014-2023 @MackRais
 *
 * @see       <https://mackrais.com>
 * @date      23.12.23
 */
namespace App\Repository;

use App\Dto\BankExchangeRate;
use App\Entity\ExchangeRateHistory;
use App\Enum\Bank;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ExchangeRateHistory>
 *
 * @method ExchangeRateHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExchangeRateHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExchangeRateHistory[]    findAll()
 * @method ExchangeRateHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExchangeRateHistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExchangeRateHistory::class);
    }

    /**
     * @return ExchangeRateHistory[] Returns an array of ExchangeRateHistory objects
     */
    public function getByBank(Bank $bank, int $limit = 2000): array
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.bank = :val')
            ->setParameter('val', $bank->value)
            ->orderBy('q.isoCode', 'ASC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    public function clearBankHistory(Bank $bank): void
    {
        print_r('BANK ', $bank->value);
        $this->createQueryBuilder('q')
            ->delete()
            ->andWhere('q.bank = :val')
            ->setParameter('val', $bank->value)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param BankExchangeRate[] $exchangeRates
     * @param Bank $bank
     * @return void
     */
    public function saveBankHistory(array $exchangeRates, Bank $bank): void
    {
        $batchSize = 20;
        $em = $this->getEntityManager();
        sort($exchangeRates);

        foreach ($exchangeRates as $key => $rate) {
            $entity = (new ExchangeRateHistory())
                ->setBank($bank)
                ->setIsoCode($rate->isoCode)
                ->setBuy($rate->buy)
                ->setSale($rate->sale)
                ->setUpdateAt();

            $em->persist($entity);

            if (($key % $batchSize) === 0) {
                $em->flush();
                $em->clear();
            }
        }

        $em->flush();
        $em->clear();
    }
}
